<?php require 'control/login.php' ?>
<?php require 'common/header.php' ?>


<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
</head>
<body>

  <div style="margin-top: 100px">
    <div class="container">
      <div class="row">
        <div class="col-md-4"></div>
         <div class="col-md-4">
           <div class="card">
             <form  method="post" id="login-form" style="margin: 20px">

            <h5 style="text-align:center"><b>ເຂົ້າສູ່ລະບົບ</b></h5><hr />

            <?php
        if(isset($msg)){
          echo $msg;
        }
        ?>

            <div class="form-group">
            <input type="email" class="form-control" placeholder="ອີເມວ" name="email" required />
            <span id="check-e"></span>
            </div>

            <div class="form-group">
            <input type="password" class="form-control" placeholder="ລະຫັດຜ່ານ" name="password" required />
            </div>

          <hr />
                    <a href="register.php">.</a>
            <div class="form-group">
              <center>
                <button type="submit" class="btn btn-info" name="btn-login" id="btn-login">
                  <span class="glyphicon glyphicon-log-in"></span> &nbsp; ເຂົ້າສູ່ລະບົບ
               </button>
              </center>
                <!-- <a href="register.php" class="btn btn-default" style="float:right;">Sign In</a> -->
            </div>
          </form>
        </div>
         </div>
       <div class="col-md-4"></div>
      </div>
    </div>
</div>

</body>
</html>


<?php require 'common/footer.php' ?>
