<?php require 'control/admin.php' ?>
<?php require 'common/header.php' ?>
<?php require 'control/dbconnect.php' ?>

<section style="margin-top: 10px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <h4 style="margin: 10px;text-align: center;"><b>ລະບົບຂໍ້ມູນອາຈານພາກວິຊາວິສະວະກຳຄອມພີວເຕີ ແລະ ໄອທີ</b></h4>
        </div>
      </div>
       <div class="col-md-6" style="margin-top: 10px">
         <a href="add_teacher.php" class="btn btn-info" style="float: left;">Insert</a>
       </di v>
       <div class="col-md-6" style="margin-top: 10px">
         <a href="logout.php?logout" class="btn btn-danger" style="float: right;">Logout</a>
       </div>
          <!-- <div class="col-md-4">
            
          </div>
          <div class="col-md-4">
            <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
          </div>
          <div class="col-md-4">
            
          </div> -->
    </div>
  </div>
</section>
<section style="margin-top: 15px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-dark">
          <tr>
            <th>#</th>
            <th>ຮູບພາບ</th>
            <th>ຊື່ ແລະ ນາມສະກຸນ</th>
            <th>ວຸດທີການສືກສາ</th>
            <th>ສາຂາວິຊາ</th>
            <th>ເລກລະຫັດ</th>
            <th>ຈັດການ</th>
          </tr>
<?php

$query = "SELECT * FROM `teacher`" ;
$result = mysqli_query($DBcon, $query);
$rows=0;
if($result->num_rows > 0 ){
  while($row = $result->fetch_assoc()) {
    if ($rows == 0){
        echo "<div class='row'>";
     }
?>
          <tr>
            <td><?php echo $row["teacher_id"]; ?></td>
            <td  class=""><a href="#"><img src="<?php echo "../img/uploads/".$row["teacher_img"]; ?>" alt="Card image cap" class="card-img-top" style="width: 5rem;"></a></td>
            <td><?php echo $row["name_teacher"]; ?></td>
            <td><?php echo $row["study_teacher"]; ?></td>
            <td><?php echo $row["subject_teacher"]; ?></td>
            <td><?php echo $row["id_teacher"]; ?></td>
            <td>
              <b><a href="insert/delete-contact.php?  teacher_id=<?php echo $row["teacher_id"]; ?>">ລືບ</a>   |  
             <a href="edite-teacher.php?teacher_id=<?php echo $row["teacher_id"]; ?>">ແກ້ໄຂ</a>  |
             <a href="view.php?teacher_id=<?php echo $row["teacher_id"]; ?>">ຂໍ້ມູນ</a>
             </b>
           </td>
          </tr>
<?php
  if($rows == 3){
       echo '</div>';
       $rows = -1;
    }
    $rows++; ?>

  <?php } }
  ?>
        </table>
      </div>
    </div>
  </div>
</section>

<?php require 'common/footer.php' ?>
