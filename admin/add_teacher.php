<?php require 'control/admin.php' ?>
<?php require 'common/header.php' ?>
<?php require 'control/dbconnect.php' ?>

<section style="margin-top: 30px">
 <div class="container">
  <a href="home.php" class="btn btn-info" style="margin-bottom: 5px; padding-left: 15px;padding-right: 15px"><h4><i class="glyphicon glyphicon-arrow-left">   Back</i></h4></a>
   <div class="row">
     <div class="col-md-6">
       <div class="card" style="text-align:center">
         <a href="#"><h1><i class="fa fa-picture-o" aria-hidden="true"></i></h1>
         <img src="../img/pic.png" style="margin: 10px">
        </a>
       </div>
     </div>

     <div class="col-md-6">
       <div class="card">
         <form action="insert/insert-teacher.php" method="post" enctype="multipart/form-data" style="margin: 10px">
           <div class="form-group">
             <label for="exampleInputEmail1">ຊື່ ແລະ ນາມສະກຸນ</label>
             <input type="text" class="form-control" name="name"  placeholder="ຊື່ ແລະ ນາມສະກຸນ" required>
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">ຮູບພາບ</label>
             <input type="file" class="form-control" name="img_menu" id="src_img"  placeholder="ຊື່ ແລະ ນາມສະກຸນ" required>
           </div>
           <div class="form-group">
             <label for="exampleInputPassword1">ວຸດທີການສືກສາ</label>
             <input type="text" class="form-control" name="study"  placeholder="ວຸດທີການສືກສາ" required>
           </div>
           <div class="form-group">
             <label for="exampleInputPassword1">ສາຂາວິຊາ</label>
             <input type="text" class="form-control" name="subject"  placeholder="ສາຂາວິຊາ" required>
           </div>
           <div class="form-group">
             <label for="exampleInputPassword1">ເລກລະຫັດ</label>
             <input type="text" class="form-control" name="idteacher"  placeholder="ເລັກລະຫັດ" required>
           </div>
           <center>
             <button type="submit" class="btn btn-info">ລົງທະບຽນ</button>
           </center>
         </form>
       </div>
     </div>
   </div>
 </div>
</section>