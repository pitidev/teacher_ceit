<?php require 'header.php' ?>
<?php require 'admin/config.php' ?>
<section style="margin-top: 10px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <p style="margin: 3px;text-align: center;">
            <img src="img/logo_FOE-NUOL_transparent.png" alt="" width="100px" style="margin-top: 5px">
            <!-- <p style="text-align: center;">ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p> -->
            <p style="text-align: center;">ຄະນະວິສະວະກຳສາດ</p>
            <b style="text-align: center;">ອາຈານພາກວິຊາ: ວິສະວະກຳຄອມພິວເຕີ ແລະ ເຕັກໂນໂລຊີຂໍ້ມູນຂ່າວສານ</b>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<section style="margin-top: 15px;margin-bottom: 20px;">
  <div class="container">
    <div class="row">

<?php
$query = "SELECT * FROM `teacher`" ;
$result = mysqli_query($conn, $query);
$rows=0;
if($result->num_rows > 0 ){
  while($row = $result->fetch_assoc()) {
    if ($rows == 0){
        echo "";
     }
?>

      <div class="col-md-6" style="margin-top: 10px">
         <div class="card">
           <div class="row">
             <div class="col-md-6">
             <div class="card">
              <img src="<?php echo "img/uploads/".$row["teacher_img"]; ?>" class="rounded float-left" alt="..." width="252px">
             </div>
            </div>
            <div class="col-md-6">
             <div class="" style="margin: 5px">
              <div class="card">
                <p style="margin: 5px">ຊື່ ແລະ ນາມສະກຸນ: 
                  <b style="color: #009688"> <?php echo $row["name_teacher"]; ?></b>
                </p>
              </div>
              <div class="card" style="margin-top: 5px">
                <p style="margin: 5px">ວຸດທິການສືກສາ: 
                  <b style="color: #009688"> <?php echo $row["study_teacher"]; ?></b>
                </p>
              </div>
              <div class="card" style="margin-top: 5px">
                <p style="margin: 5px">ສາຂາວິຊາ: 
                  <b style="color: #009688"> <?php echo $row["subject_teacher"]; ?></b> 
                </p>
              </div>
              <div class="card" style="margin-top: 5px">
                <p style="margin: 5px">ເລກລະຫັດ: 
                  <b style="color: #009688"> <?php echo $row["id_teacher"]; ?></b>
                </p>
              </div>
             </div>
            </div>
           </div>
         </div>
       </div>

<?php
  if($rows == 3){
       echo '';
       $rows = -1;
    }
    $rows++; ?>
  <?php } }
  ?>

    </div>
  </div>
</section>

  </body>
</html>
